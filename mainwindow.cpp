#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myglwidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->sbFOV, qOverload<int>(&QSpinBox::valueChanged), ui->openGLWidget, &MyGLWidget::setFOV);
    connect(ui->sbAngle, qOverload<int>(&QSpinBox::valueChanged), ui->openGLWidget, &MyGLWidget::setAngle);
    connect(ui->sbFOV, qOverload<int>(&QSpinBox::valueChanged), this, [this](){qDebug() << "FOV: " << ui->openGLWidget->getFOV();});
    connect(ui->sbAngle, qOverload<int>(&QSpinBox::valueChanged), this, [this](){qDebug() << "Angle: " << ui->openGLWidget->getAngle();});

    connect(ui->dsbFar, qOverload<double>(&QDoubleSpinBox::valueChanged),ui->openGLWidget, &MyGLWidget::setFar);
    connect(ui->dsbNear, qOverload<double>(&QDoubleSpinBox::valueChanged),ui->openGLWidget, &MyGLWidget::setNear);
}

MainWindow::~MainWindow()
{
    delete ui;
}

