#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QOpenGLWidget>
#include <QWidget>
#include <QVector3D>
#include <QEvent>
#include <QKeyEvent>
#include <QDebug>

class MyGLWidget : public QOpenGLWidget
{
public:
    MyGLWidget(QWidget*);
    ~MyGLWidget();

public slots:

    void setFOV(int value);
    void setAngle(int value);
    void setProjectionMode();
    void setNear(double value);
    void setFar(double value);
    void setRotationA(int value);
    void setRotationB(int value);
    void setRotationC(int value);
    int getFOV();
    int getAngle();
public:
    void keyPressEvent(QKeyEvent *event);

private:
    int m_FOV;
    int m_Angle;
    double m_Near;
    double m_Far;
    int m_RotationA;
    int m_RotationB;
    int m_RotationC;
    QVector3D m_CameraPos;
};

#endif // MYGLWIDGET_H
