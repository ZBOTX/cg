#include "myglwidget.h"


MyGLWidget::MyGLWidget(QWidget *parent): QOpenGLWidget(parent){

    setFocusPolicy(Qt::StrongFocus);

    m_FOV = 0;
    m_Angle = 0;
    m_CameraPos = QVector3D();

}

MyGLWidget::~MyGLWidget(){};
void MyGLWidget::setFOV(int value){
    m_FOV = value;
};
void MyGLWidget::setAngle(int value){
    m_Angle = value;
};
void MyGLWidget::setProjectionMode(){};
void MyGLWidget::setNear(double value){
    m_Near = value;
};
void MyGLWidget::setFar(double value){
    m_Far = value;
};
void MyGLWidget::setRotationA(int value){
    m_RotationA = value;
};
void MyGLWidget::setRotationB(int value){
    m_RotationB = value;
};
void MyGLWidget::setRotationC(int value){
    m_RotationC = value;
};


int MyGLWidget::getFOV(){
    return m_FOV;
}
int MyGLWidget::getAngle(){
    return m_Angle;
}


void MyGLWidget::keyPressEvent(QKeyEvent *event){
    if (event->key() == Qt::Key_W){
        m_CameraPos.setY(m_CameraPos.y() + 0.2f);
    }
    if (event->key() == Qt::Key_A){
        m_CameraPos.setX(m_CameraPos.x() - 0.2f);

    }
    if (event->key() == Qt::Key_S){
        m_CameraPos.setY(m_CameraPos.y() - 0.2f);

    }
    if (event->key() == Qt::Key_D){
        m_CameraPos.setX(m_CameraPos.x() + 0.2f);

    }
    else {
        QOpenGLWidget::keyPressEvent(event);
    }
    qDebug() << m_CameraPos;
}



